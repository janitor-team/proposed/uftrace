uftrace (0.12-5) unstable; urgency=medium

  * Upload to sid.

 -- Gürkan Myczko <tar@debian.org>  Thu, 06 Oct 2022 08:44:04 +0200

uftrace (0.12-4) experimental; urgency=medium

  * Disable tests as they all fail.

 -- Gürkan Myczko <tar@debian.org>  Thu, 06 Oct 2022 07:29:18 +0200

uftrace (0.12-3) experimental; urgency=medium

  * d/clean: fixed.
  * Fix tests. (Closes: #1015032)
  * Bump standards version to 4.6.1.
  * d/changelog: change the 0.10-1 entry from UNRELEASED to unstable.

 -- Gürkan Myczko <tar@debian.org>  Wed, 05 Oct 2022 15:01:02 +0200

uftrace (0.12-2) unstable; urgency=medium

  * Reactivate python patch.

 -- Gürkan Myczko <tar@debian.org>  Thu, 21 Jul 2022 14:53:04 +0200

uftrace (0.12-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Sun, 19 Jun 2022 21:24:15 +0200

uftrace (0.11-6) unstable; urgency=medium

  * Source only upload.

 -- Gürkan Myczko <tar@debian.org>  Tue, 07 Jun 2022 11:00:10 +0200

uftrace (0.11-5) unstable; urgency=low

  * Remove armhf as a build target again; still not building

 -- paul cannon <pik@debian.org>  Thu, 31 Mar 2022 20:58:38 -0500

uftrace (0.11-4) unstable; urgency=medium

  * Switch back to the original Git history (Gürkan Myczko mistakenly thought
    there was not one already, and started a new history. This revision
    includes the relevant changes from that history.)
  * Remove build-depends on python-is-python3 (dependencies on that package
    are disallowed by Debian's Python policy.)
  * Reinstate spelling and python3 shebang patches
  * use debhelper-compat instead of debian/compat
  * Fix build on experimental armhf (thanks to Nobuhiro Iwamatsu;
    closes: #1008134)
  * Uploading the 0.11 series to unstable (closes: #1007959)

 -- paul cannon <pik@debian.org>  Tue, 22 Mar 2022 11:35:12 -0500

uftrace (0.11-3) experimental; urgency=medium

  * Try to disable failing tests temporarily.

 -- Gürkan Myczko <tar@debian.org>  Tue, 15 Mar 2022 15:00:01 +0100

uftrace (0.11-2) experimental; urgency=medium

  * d/control: add build-depends for python-is-python3.

 -- Gürkan Myczko <tar@debian.org>  Tue, 15 Mar 2022 12:02:31 +0100

uftrace (0.11-1) experimental; urgency=medium

  * Add myself to Uploaders.
  * Team upload.
  * New upstream version. (Closes: #966988, #967228, #938745)
  * Bump standards version to 4.6.0.
  * Bump debhelper version to 12, drop d/compat.
  * d/clean: added.

 -- Gürkan Myczko <tar@debian.org>  Thu, 25 Nov 2021 11:14:00 +0100

uftrace (0.10-1) unstable; urgency=medium

  * New upstream release, which includes:
    - No longer requires python2 (closes: #967228, #938745)
    - Fixes FTBFS from spurious test failures (closes: 966988). Thanks
      to Logan Rosen for identifying the issue and submitting a patch,
      although I failed to address it in the 0.9.4 series.
  * Many thanks to Adrian Bunk for the NMUs releasing 0.9.4 and fixing the
    build failures.

 -- paul cannon <pik@debian.org>  Mon, 06 Sep 2021 15:54:23 -0500

uftrace (0.9.4-0.2) unstable; urgency=high

  * Non-maintainer upload.
  * Remove the autopkgtest for now, it doesn't fail when tests fail
    but fails for other reasons on arm64. (Closes: #947089)

 -- Adrian Bunk <bunk@debian.org>  Wed, 22 Apr 2020 17:06:49 +0300

uftrace (0.9.4-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release.
    - Fixes FTBFS on armhf. (Closes: #947089)

 -- Adrian Bunk <bunk@debian.org>  Mon, 13 Apr 2020 16:18:27 +0300

uftrace (0.9.3-1) unstable; urgency=low

  * New upstream release
  * Adds support for nodoc build option
  * Adds i386 as a build target
  * Make build reproducible (this mostly involved adding the
    -ffile-prefix-map compiler option)
  * Adds support for autopkgtest
  * Add Rules-Requires-Root: no
  * Bump Standards-Version to 4.4.0
  * Install private shared objects to /usr/lib/$(DEB_HOST_MULTIARCH)/uftrace

 -- paul cannon <pik@debian.org>  Wed, 18 Sep 2019 21:14:16 -0500

uftrace (0.9.0-1) unstable; urgency=medium

  * New upstream release:
    + Fixes FTBFS on buster/sid (closes: #906512).
    + Now reports correct version (closes: #886863).
  * Adds arm64 as a build target (closes: #898000, and thank you, Matthias
    Klose).
  * Add new (optional) build dependencies for the sake of features:
    pkg-config, libpython2.7-dev, libdw-dev, libncursesw5-dev
  * Bump Standards-Version to 4.2.1.

 -- paul cannon <pik@debian.org>  Tue, 11 Sep 2018 16:19:19 -0500

uftrace (0.8.2-1) unstable; urgency=low

  * New upstream release

 -- paul cannon <pik@debian.org>  Fri, 05 Jan 2018 13:23:39 -0600

uftrace (0.7.0-1) unstable; urgency=low

  * New upstream release (closes: #871596).
  * Add patch to fix calculation of message sizes under send_trace_metadata

 -- paul cannon <pik@debian.org>  Wed, 09 Aug 2017 12:08:47 -0500

uftrace (0.6.2-2) unstable; urgency=low

  * Don't claim to build on armel; the vstr instructions used in the
    ARM port for this software are not available under armel
    (closes: #858554). Thanks to Adrian Bunk.

 -- paul cannon <pik@debian.org>  Thu, 23 Mar 2017 10:18:14 -0500

uftrace (0.6.2-1) unstable; urgency=low

  * New upstream release
  * Release strategy has been established; define debian/watch
  * Use pristine-tar

 -- paul cannon <pik@debian.org>  Thu, 16 Mar 2017 12:41:12 -0500

uftrace (0.6.0.20161014-1) unstable; urgency=low

  * Initial packaging of git snapshot at commit 06e6740256 (closes: #839877).

 -- paul cannon <pik@debian.org>  Fri, 14 Oct 2016 10:55:12 -0500
